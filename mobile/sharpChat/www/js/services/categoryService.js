/**
 * Created by oDev on 1/20/2015.
 */
sharpChat.factory('CategoryService',function(AjaxService){
    var getAllCategory  = AjaxService.getAllCategory();

    return{
        get:function(successCb,errorCb){
            return getAllCategory.get(null,function(response){
                if(response.code == 200){
                    successCb(response)
                }else{
                    errorCb(response);
                }
            })
        }
    }
})