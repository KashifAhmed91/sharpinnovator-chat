/**
 * Created by kashif on 1/18/2015.
 */
sharpChat.factory('AjaxService',function($resource,$rootScope){
    var server = $rootScope.server;
    function _doLogin(){
        return $resource(server+'user/login',{email:'@email',password:'@password'});
    }

    function _doSignUp(){
        return $resource(server+'user/signUp',{name:'@name',email:'@email',password:'@password',professional:'@professional',category:'@category'});
    }

    function _getUser(){
        return $resource(server+'user/findUser',{_id:'@_id'});
    }

    function _getAllCategory(){
        return $resource(server+'user/getCategory');
    }

    function _sendCode(){
        return $resource(server+'user/sendCode',{email:'@email'});
    }

    return{
        'doLogin': _doLogin,
        'doSignUp': _doSignUp,
        'getUser': _getUser,
        'getAllCategory': _getAllCategory,
        'sendCode': _sendCode
    }
});