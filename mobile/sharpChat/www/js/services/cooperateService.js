/**
 * Created by kashif on 1/18/2015.
 */
sharpChat.factory('CooperateService',function(AjaxService,SocketService){
    var login = AjaxService.doLogin();
    var signUp = AjaxService.doSignUp();
    var sendCode = AjaxService.sendCode();
    var _user = {};
    var category = {};
    var _chatRoomInfo ={};
    return{
        user:{
            set: function(userObj){
                _user = userObj
            },
            get: function(){
                return _user;
            }
        },
        login:function(user,SuccessCb,ErrorCb){
            return login.save({email:user.email,password:user.password},function(response){
                    if(response.code==200){
                        SuccessCb(response);
                    }else{
                        ErrorCb(response.data);
                    }
            });
        },
        signUp:function(user,SuccessCb,ErrorCb){
            return signUp.save({
                name: user.name,
                email: user.email,
                password: user.password,
                professional: user.professional,
                category: user.category
            },function(response){
                if(response.code == 200){
                    SuccessCb(response);
                }else{
                    ErrorCb(response.data)
                }
            });
        },
        sendCode: function(email,successCb,errorCb){
            return sendCode.save({email:email},function(response){
                if(response.code==200 && response.user){
                        successCb("Successfully Send Code")
                }else{
                    errorCb("Invalid Email")
                }
            })
        },
        category:{
            get: function () {
                return category;
            },
            set: function(categoryObj){
                category = categoryObj
            }
        },
        socket: {
            startChat: function(callBack){
                SocketService.on('startChat',function(data){
                    callBack(data);
                })
            }
        },
        chatRoomInfo: {
            get: function(){
                return _chatRoomInfo;
            },
            set: function(obj){
                _chatRoomInfo = obj;
            }
        }
    }
});