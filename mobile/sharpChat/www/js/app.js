// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var sharpChat = angular.module('sharpChat', ['ionic','ngResource','btford.socket-io'])

    .run(function($ionicPlatform,$rootScope) {
        /*Local Server*/
        $rootScope.server = "http://localhost:8080/";

        //$rootScope.io = io.connect("http://localhost:8080/")
        //$rootScope.server = "https://sharpchatapi.herokuapp.com/";
        $rootScope.io = io.connect($rootScope.server);




        /*Deployed Server*/

        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if(window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if(window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    }).config(function($stateProvider,$urlRouterProvider) {



        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: 'partials/home.html',
                controller:'loginController'
            })
            .state('localLogin',{
                url:'/localLogin',
                templateUrl: 'partials/localLogin.html',
                controller:'localLoginController'
            })
            .state('forgetPass',{
                url:'/forgetPass',
                templateUrl: 'partials/forgetPass.html',
                controller:'forgetPassController'
            })
            .state('signUp',{
                url:'/signUp',
                templateUrl: 'partials/signUp.html',
                controller: 'signUpController'
            })
            .state('user',{
                url: '/user',
                controller: 'userController',
                templateUrl:'partials/user.html'
            })
            .state('user.categoryList',{
                url: '/categoryList',
                templateUrl: 'partials/categoryList.html',
                controller:'categoryListController'
            })
            .state('user.category',{
                abstract: true,
                url: '/category',
                controller: 'categoryController',
                templateUrl: 'partials/category.html'
            })
            .state('user.category.main',{
                url: '/main',
                controller: 'categoryMainController',
                templateUrl: 'partials/categoryMain.html'
            })
            .state('user.category.professionalList',{
                url: '/professionalList',
                templateUrl: 'partials/professionalList.html',
                controller: 'professionalListController'
            })
            .state('user.category.topicDiscussion',{
                url: '/categoryTopic',
                templateUrl: 'partials/topics.html',
                controller: 'topicsController'
            })
            .state('professional',{
                abstract: true,
                url: '/professional',
                templateUrl:'partials/professional.html',
                controller:'professionalController'
            })

            .state('professional.openChatList',{
                url: '/openChat',
                templateUrl:'partials/openChatList.html',
                controller:'openChatListController'
            })
            .state('professional.openTopics',{
                url: '/openTopics',
                templateUrl:'partials/openTopics.html',
                controller:'openTopicsController'
            })
            .state('professional.topicDiscussion',{
                url:'/topic',
                templateUrl:'partials/topicDiscussion.html',
                controller:'topicDiscussionController'
            })
            .state('chats',{
                url:'/chats',
                templateUrl: 'partials/chats.html',
                controller:'chatsController'
            })
            /*.state('topics',{
                url:'/topics',
                templateUrl: 'partials/topics.html',
                controller:'topicsController'
            })*/
            .state('particularChat',{
                url:'/particularChat',
                templateUrl: 'partials/particularChat.html',
                controller:'particularChatController'
            })

    }).controller('chatDiv',function($rootScope,$scope,SocketService) {

        SocketService.on('connected', function (msg) {
            console.log(msg)

        });
        SocketService.on('RequestForChat', function (msg) {
            //requestingUserData._id,requestingUserData.name

            //here you will generate popup to ask professional for chat

            //if(yes){
            //    SocketService.emit('chatRequestAccepted',{memberID:{memberOne:prof,memberTwo:use}})
            //}else{
            //    SocketService.emit('chatRequestCancelled',{})
            //
            //}


        });
        SocketService.on('RequestForResumeChat', function (msg) {
            console.log('RequestForResumeChat');
            // socketTest.emit('joinTheRoom',msg)

        });
        SocketService.on('message', function (msg) {
            console.log('mg1')
            //var Url = $location.$$path;
            //if(Url == '/inbox/message/'+msg.data.memberID.memberOne)
            //{}else{
            //    $location.path('/inbox/message/'+msg.data.memberID.memberOne)
            //    if(!$scope.$$phase) $scope.$apply();
            //}
        })

        SocketService.on('update', function (msg) {
            console.log(msg)
        })

        SocketService.on('error', function (msg) {
            console.log(msg)
        })

        SocketService.on('reconnectSocket', function (msg) {


        })



        SocketService.on('listOfOnlineUser', function (clients) {
            console.log(clients)
        });
    });


