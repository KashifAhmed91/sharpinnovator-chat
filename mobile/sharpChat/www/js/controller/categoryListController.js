/**
 * Created by kashif on 1/19/2015.
 */
sharpChat.controller('categoryListController',function($scope,$state,CategoryService,CooperateService){
    $scope.users = [

    ];
    CategoryService.get(function(list){
        $scope.categorList = list.data;
    },function(error){
            console.log(error);
    })
    $scope.changeState = function (state,categoryObj) {
        CooperateService.category.set(categoryObj);
        $state.go(state);
    }
})