/**
 * Created by kashif on 1/18/2015.
 */
sharpChat.controller('localLoginController',function($scope,CooperateService,$state,SocketService){
    $scope.user = {};
    $scope.doLogin = function(){
        CooperateService.login($scope.user,function(res){
            if(res.data==null){
                alert("Incorrect Email or Password");
            }else{
                if(!res.data.professional){
                    var socket= SocketService.socket();
                    SocketService.emit('addMeToSocket',{userID:res.data._id,socketID:socket.id,key:'U'});
                    SocketService.on('userAdded',function(data) {
                        alert("User");
                        console.log(data);
                        CooperateService.user.set(data.userData.userID);
                        $state.go('user.categoryList')
                    })
                }
                else{
                    alert("You are professional You Only Access through Web")

                }
            }
        },function(err){
            console.log(err)
        });
    };

    $scope.changeState = function(state){
        $state.go(state)
    }
});