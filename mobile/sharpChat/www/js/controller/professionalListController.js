/**
 * Created by oDev on 1/21/2015.
 */
sharpChat.controller('professionalListController',function($scope,CooperateService,$state,SocketService){
    $scope.professionals = CooperateService.category.get().members;
    $scope.professionals.forEach(function(professional){
        professional.available = false;
    });
    $scope.onlineProfessionalList = [];
    $scope.changeState = function(stata,professional){
        $state.go()
    };

    $scope.reqforChat = function(profId){
        //var userID ='54bf8a46c7b4ef740dd037ee'; //k@user.com
        //var profID ='54bfb82a63f4d10300acc0af';    //ali@ali.com
        SocketService.emit('sendChatRequest',{userID:CooperateService.user.get(),profID:profId})
    };



    SocketService.on('listOfOnlineUser',function(onlineUsers){
        console.log('Online Users');
        console.log(onlineUsers);
        onlineUsers.forEach(function(onlineProfessional){
           $scope.professionals.forEach(function(groupProfessional){
               if(onlineProfessional.userID==groupProfessional.id){
                   /*$scope.onlineProfessionalList.push({
                       name: groupProfessional.name,
                       id: groupProfessional.id,
                       socketId: onlineProfessional.socketID
                   })*/
                   groupProfessional.available = true;
                   groupProfessional.socketId = onlineProfessional.socketID
               }
           })
        });
        console.log('Online Users');
    });

    CooperateService.socket.startChat(function(room){
        CooperateService.chatRoomInfo.set(room);
        $state.go('particularChat')
    })
});