/**
 * Created by kashif on 1/18/2015.
 */
sharpChat.controller('signUpController',function($scope,CooperateService,$state,CategoryService){
    $scope.user = {};
    $scope.list=[];
    $scope.selectedCategory = {};

    $scope.categoryList = (function(){
        CategoryService.get(function(category){
            console.log("Success",category);
            $scope.categories=category.data;
        },function(error){
            console.log("Error",error);
        })
    })();

    $scope.doSignUp = function(){
        if($scope.user.password==$scope.user.confirmPassword){
            CooperateService.signUp($scope.user,function(res){
                $state.go('index');
            },function(res){
                if(res.code == 11000){
                    console.log("User Already Exist");
                }else{
                    console.log("Try Again")
                }
            })
        }else{
            alert("Password Not Match");
            $scope.user = {}
        }

    };

    $scope.changeCategory = function (selectedCategory) {
        $scope.user.category = {
            name: selectedCategory.name,
            id: selectedCategory._id
        }
    };

    $scope.$watch('user.professional',function(newValue){
        if(!newValue){
            $scope.user.category = {};
            console.log('Disable Category');
        }else{
            console.log('Enable Category');
        }
    });
});