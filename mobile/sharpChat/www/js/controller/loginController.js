/**
 * Created by M.JUNAID on 2015-01-17.
 */
sharpChat.controller('loginController',function($scope, $ionicPopup, $timeout,AjaxService,$state,socketFactory) {
    //var myIoSocket = socketFactory();
    //myIoSocket.connect('http://localhost:8080/')
    ////mySocket = socketFactory({
    //    ioSocket: myIoSocket
    //});
    //console.log(myIoSocket)
    // Triggered on a button click, or some other target
    $scope.showPopup = function() {
        $scope.data = {};

        // An elaborate, custom popup
        var myPopup = $ionicPopup.show({
            template: '<input type="radio" name="data" ng-model="data.wifi" value="User">User<br>' +
            '<input name="data" type="radio" ng-model="data.wifi" value="Professional">Professional',
            title: 'Select Your Category',
            scope: $scope,
            buttons: [
                { text: 'Cancel' },
                {
                    text: '<b>Ok</b>',
                    type: 'button-positive',
                    onTap: function(e) {
                        if (!$scope.data.wifi) {
                            //don't allow the user to close unless he enters wifi password
                            e.preventDefault();
                        } else {
                            return $scope.data.wifi;
                        }
                    }
                }
            ]
        });
        myPopup.then(function(res) {
            console.log('Tapped!', res);
        });
        $timeout(function() {
            myPopup.close(); //close the popup after 3 seconds for some reason
        }, 20000);
    };

    $scope.changeState = function(state){
        $state.go(state)
    }
});
