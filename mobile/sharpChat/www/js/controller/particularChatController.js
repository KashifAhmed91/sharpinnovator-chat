sharpChat.controller('particularChatController',function($scope, $state,CooperateService,SocketService){
    $scope.changeState = function(state){
        $state.go(state)
    }

    console.log('chat Info',CooperateService.chatRoomInfo.get());
    console.log('chat client Id',CooperateService.user.get());

    $scope.messageData={}

    $scope.sendMessage =function(){
        var roonmInfo = CooperateService.chatRoomInfo.get()
        $scope.messageData.senderID = CooperateService.user.get()
        $scope.messageData.reciverID = roonmInfo.userIDFrom;
        $scope.messageData.roomID = roonmInfo.roomID;

        SocketService.emit('HostSendMessage',$scope.messageData)

    }

    SocketService.on('messageDelivered',function(data){
        console.log(data.msg)
        console.log(data.data)
    })
    SocketService.on('message',function(data){
        console.log(data.msg)
        console.log(data.data)
    })

    SocketService.on('errorInSendingMessage',function(data){
        console.log(data.msg)
        console.log(data.data)
    })

});