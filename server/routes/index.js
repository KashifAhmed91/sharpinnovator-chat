/**
 * Created by oDev on 1/16/2015.
 */
exports = module.exports = function(app){
    app.post('/admin/login',app.adminLogin);
    app.post('/user/login',app.userLogin);
    app.post('/user/signUp',app.userSignUp);
    app.post('/user/findUser',app.getUser);
    app.get('/user/getUsers',app.getAllUser);
    app.post('/user/sendCode',app.sendCode);

    app.get('/user/getCategory',app.getAllCategory);
    app.post('/user/setCategory',app.setCategory);
    app.post('/user/deleteCategory',app.removeCategory);

    app.post('/user/createTopic',app.createTopic);

}