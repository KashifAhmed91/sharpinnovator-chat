/**
 * Created by oDev on 1/16/2015.
 */
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var app = express();
var http = require('http');



app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
var port = process.env.PORT || 8080;
app.use(function(req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("'Access-Control-Allow-Methods',['OPTIONS', 'GET', 'POST', 'DELETE']");
    res.header("'Access-Control-Allow-Headers','Content-Type'");

    next();
});

require('./config')(app);
require('./db')(app,mongoose);
require('./model')(app,mongoose);
require('./apis')(app);
require('./routes')(app);


/*app.listen(port,function(){
    console.log("Server is Listen on 8080");
});*/

var server =  http.createServer(app).listen(port);
var io = require('socket.io').listen(server);

// Reduce the logging output of Socket.IO
io.set('log level',1);


io.sockets.on('connection', function (socket) {
    console.log('client connected' + socket.id);
    socket.emit('connected', {message: "You are connected!"});
    app.initChat(io, socket);


    //socket.emit('connected', { message: "You are connected!" });

    // Host Events

    socket.on('addMeToSocket', app.addMeToSocket);
    socket.on('sendChatRequest', app.sendChatRequest);
    socket.on('chatRequestCancelled', app.chatRequestCancelled);
    socket.on('chatRequestAccepted', app.chatRequestAccepted);

      socket.on('HostSendMessage', app.HostSendMessage);
  //  socket.on('checkSocketConnection', app.checkSocketConnection)
    socket.on('disconnect', function () {
        app.disconnect(io, socket)
    });
})