/**
 * Created by oDev on 1/24/2015.
 */
exports = module.exports = function(app,mongoose){
    var Schema = mongoose.Schema;

    var TopicSchema = new Schema({
        name:{type:String, required:true},
        discussion:[
            {
                user:{
                    name: {type:String,required:true},
                    profilePic: {type: String}
                },
                post: {
                    text: {type: String},
                    images: [{
                        img:{type:String}
                    }]
                }
            }
        ]
    });

    app.db.model('Topic',TopicSchema);
};