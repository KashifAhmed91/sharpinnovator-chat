/**
 * Created by oDev on 1/17/2015.
 */
exports = module.exports = function(app,mongoose){

    var Schema = mongoose.Schema;

    var AuthSchema = new Schema({
        name: {type: String,required: true},
        email: {type: String,required: true,unique: true},
        password: {type: String, required: true},
        professional: {type: Boolean, default: false},
        admin: {type: Boolean, default: false},
        generatedTime: { type : Date, default : Date.now },
        category: {
            name: {type: String},
            id: {type: Schema.ObjectId}
        }

    });

    app.db.model('Auth',AuthSchema);
};


