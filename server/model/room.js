/**
 * Created by kashif on 1/25/2015.
 */
exports = module.exports = function(app,mongoose){
    var Schema = mongoose.Schema;

    var RoomSchema = new Schema({
        roomID:{type:String, required:true},
        memberID:{type:Array,required:true},
        chatEnd:{type: Boolean, default:false}
    });

    app.db.model('Room',RoomSchema);
};