/**
 * Created by oDev on 1/17/2015.
 */
exports = module.exports = function(app,mongoose){
    require('./auth')(app,mongoose);
    require('./category')(app,mongoose);
    require('./topic')(app,mongoose);
    require('./room')(app,mongoose);
};