/**
 * Created by oDev on 1/19/2015.
 */
exports = module.exports = function(app,mongoose){
    var Schema = mongoose.Schema;

    var CategorySchema = new Schema({
        name: {type:String,required:true},
        description: {type:String,required:true},
        members: [{
            name: {type:String},
            id: {type: Schema.ObjectId}
        }]
    });

    app.db.model('Category',CategorySchema);
};