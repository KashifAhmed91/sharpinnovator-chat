/**
 * Created by oDev on 1/16/2015.
 */
exports = module.exports = function(app){
    app.adminLogin = function(req,res){
        if(req.body.email == "admin@sharpchat.com" && req.body.pass == "doctor"){
            res.send({
                code:200
            })
        }else{
            res.send({
                code:404
            })
        }
    };

    app.userLogin = function(req,res){
        var userQuery = app.db.models.Auth.where({email:req.body.email,password:req.body.password});
        userQuery.findOne(function(err,user){
            if(!user){
                res.send({
                    code: 404
                })
            }else{
                res.send({
                    code:200,
                    data: user
                })
            }
        });

    };

    app.userSignUp = function(req,res){
        var userSignUpSchema = new app.db.models.Auth(req.body);
        userSignUpSchema.save(function(err,newSignUpUser){
            if(err){
                res.send({
                    code:err.code,
                    data:err
                });
            }else{
                if(newSignUpUser._doc.professional){

                    // If the user is professional then update the members in category
                    app.db.models.Category.findByIdAndUpdate(
                        {
                            _id: newSignUpUser._doc.category.id
                        },
                        {
                            $push: {
                                members: {
                                    name: newSignUpUser._doc.name,
                                    id: newSignUpUser._doc._id
                                }
                            }
                        },
                        {
                            safe: true,
                            upsert: true
                        },
                        function(err, model) {
                            if(err){
                                res.send({
                                    code:err.code,
                                    data:err
                                })
                            }else{
                                res.send({
                                    code:200,
                                    data:model
                                });
                            }
                        }
                    );
                }else{
                    res.send({
                        code:200,
                        data:newSignUpUser
                    });
                }

            }
        })
    };

    app.getAllUser = function(req,res){
        app.db.models.Auth.find({},function(err,user){
            if(err){
                res.send({
                    code:err.code,
                    err:err
                })
            }else{
                res.send({
                    code: 200,
                    data: user
                })
            }
        })
    }

    app.getUser = function(req,res){
        app.db.models.Auth.findOne({_id:req.body._id},'_id name',function(err,userName){
            if(err){
                res.send({
                    code:err.code,
                    err:err
                })
            }else{
                res.send({
                    code: 200,
                    name: userName
                })
            }

        })
    };

    app.sendCode = function(req,res){
        app.db.models.Auth.findOne({email:req.body.email},'email',function(err,user){
            if(err){
                res.send({
                    code:err.code,
                    err:err
                })
            }else{
                console.log(user);
                res.send({
                    code:200,
                    user:user
                })
            }
        })
    }
};