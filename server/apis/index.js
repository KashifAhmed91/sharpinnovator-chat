/**
 * Created by oDev on 1/16/2015.
 */
exports = module.exports = function(app){
    require('./auth')(app);
    require('./category')(app);
    require('./topic')(app);
    require('./socketJs')(app);
    require('./room')(app);
};