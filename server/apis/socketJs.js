exports = module.exports = function(app){

    var clients=[];
    var localRooms=[];
    var qstatus = "";
    var Q = require('q');
    var sessionMgm = require('./sessionManagement');


    app.initChat = function(sio, socket){
        io = sio;
        //setInterval( sendClientList, 5000);
    }


    app.addMeToSocket =  function(data){
        var user = sessionMgm.getSessionByUserID(data.userID);
        if(user  == null){
            sessionMgm.add({userID:data.userID,socketID:data.socketID,key:data.key});
            this.emit('userAdded',{userData:data});
            io.sockets.emit('listOfOnlineUser', sessionMgm.getAllMember());
        }else{
            this.emit('User is already there in soio.socketscket',data);
        }

    };

    app.sendChatRequest =  function(data){



        var prof= sessionMgm.getSessionByUserID(data.profID);
        if(prof != null){

            app.db.models.Auth.findOne({_id:data.userID},'_id name',function(err,res){
                //console.log(res);
                io.sockets.connected[prof.socketID].emit('RequestForChat', {requestingUserData:res});
                //  io.sockets.socket(prof.socketID).emit('RequestForChat',{userIDFrom:res,name:"asdasd"});
            })
        }else{
            this.emit('Professional you are trying to reach is currently offline',data);
        }


    };

    app.chatRequestCancelled = function(data){
        var user= sessionMgm.getSessionByUserID(data.memberID.memberTwo);
        if(user != null){
            app.db.models.Auth.findOne({_id:data.memberID.memberTwo},'_id name',function(err,res){
                // console.log(res);
                io.sockets.connected[user.socketID].emit('chatRequestCancelledByProf', {cancellingProfData:res});
                //  io.sockets.socket(prof.socketID).emit('RequestForChat',{userIDFrom:res,name:"asdasd"});
            })
        }else{
            this.emit('Professional you are trying to reach is currently offline',data);
        }

    }


    function SerachRoomInDB(data){
        var deferred = Q.defer();
        var result ;
        app.db.models.Room.findOne({ membersID: { $all: [ data.memberID.memberOne, data.memberID.memberTwo ] } },function(err,room){
            if (err) {// ...
                //console.log('An error has occurred');
                result = deferred.resolve({status:400,msg:'An error has occurred'})

            }
            else {
                if(!room){
                    result = deferred.resolve({status:201})
                }else{
                    if(room.chatEnd == true){
                        result = deferred.resolve({status:303,room:room})
                    }else{
                        result =  deferred.resolve(room);
                    }

                }
            }

        });
        return deferred.promise;

    }

    var saveRoom = function(data,profSocket,user){

        //console.log('room not found');
        var thisRoomId = ( Math.random() * 100000 ) | 0;
        var roomInfo = {roomID:'chatRoom'+thisRoomId,memberID:[data.memberID.memberOne,data.memberID.memberTwo]}
        var query = new app.db.models.Room(roomInfo)
        query.save(function(err,roomSave){
            if(err){
                // console.log(qstatus = {status:400,msg:"Room Addition Fail"+err})
            }else{
                //console.log({status:200,msg:"Room Added"+roomSave})


                io.sockets.connected[user.socketID].emit('startChat',{userIDFrom:data.memberID.memberOne,roomID:roomInfo.roomID});
                profSocket.room = roomInfo.roomID
                localRooms.push(roomInfo)
                profSocket.emit('startChat',roomInfo);
                profSocket.join(roomInfo.roomID);

            }
        });//>save

    }


    var resumeChat = function(room,data,profSocket,user){
        //console.log('resumeChat'+userSocket.id)

        var tempRoomInfo = {roomID:room.roomID,membersID:room.membersID}
        var roomCheck = localRooms.filter(function(localroom) {
            return localroom.roomID ===room.roomID; // filter out appropriate one
        });
        if(roomCheck.length == 1){
            profSocket.room =(room.roomID)
            profSocket.join((room.roomID));
            profSocket.emit('startChat',tempRoomInfo);
            io.sockets.connected[user.socketID].emit('startChat',{userIDFrom:data.memberID.memberOne,roomID:roomInfo.roomID});
        }else{
            profSocket.room = (room.roomID)
            profSocket.join((room.roomID));
            profSocket.emit('startChat',tempRoomInfo);
            localRooms.push(tempRoomInfo)
            io.sockets.connected[user.socketID].emit('startChat',{userIDFrom:data.memberID.memberOne,roomID:roomInfo.roomID});

        }



    }

    app.chatRequestAccepted = function(data) {
        var user= sessionMgm.getSessionByUserID(data.memberID.memberTwo);
        var profSocket = this;
        if(user != null){
            SerachRoomInDB(data).then(function(room) {
                if (room.status == 400) {
                    this.emit('error', err);
                } else if (room.status == 201) {
                    return saveRoom(data, profSocket,user)
                } else if (room.status == 303) {
                    return saveRoom(data, profSocket,user)
                } else {
                    return resumeChat(room, data, profSocket,user)
                }
            })

        }else{
            this.emit('Professional you are trying to reach is currently offline',data);
        }
    }

    app.HostSendMessage = function(data){
        //console.log(data)
     var senderSocket = this;
        var roomCheck = localRooms.filter(function(localroom) {
            return localroom.roomID ===data.roomID; // filter out appropriate one
        });
        if(roomCheck.length == 1){
        var user= sessionMgm.getSessionByUserID(data.reciverID);
        if(user != null){
            senderSocket.emit('messageDelivered', {msg:'messageDelivered',data:data});
            io.sockets.connected[user.socketID].emit('message',{msg:'message',data:data});
        }else{
            senderSocket.emit('errorInSendingMessage', {msg:'The person you are trying to reach is offline message',data:data});

        }
        }else{
            senderSocket.emit('errorInSendingMessage',{msg:'the chat is turn off',data:data});

        }
    }


    app.disconnect = function (io,socket){

        var user = sessionMgm.getSessionBySocketID(socket.id);
        if(user !== null){
            //clients.splice(clients.indexOf(userCheck[0]), 1);
            sessionMgm.remove(socket.id)
            io.sockets.emit('listOfOnlineUser', sessionMgm.getAllMember());
            // console.log(clients)
        }
        //clients.splice(clients.indexOf(data), 1);

    }

    app.checkSocketConnection =  function(data){
        if(this.id == data.socketID){
            // console.log('sessionID match')
        }else{

            var user = sessionMgm.getSessionByUserID(data.userID);
            if(user !== null){
                if(user.socketID == this.id){
                    console.log('same sessinID')
                }else{
                    sessionMgm.updateSocketID(user,this.id)
                    //clients[clients.indexOf(userCheck[0])].sessionID = data.socketID
                    io.sockets.emit('listOfOnlineUser',  sessionMgm.getAllMember());
                    //console.log(clients)
                }

            }else{
                sessionMgm.add({userID:data.userID,socketID:this.id})
                io.sockets.emit('listOfOnlineUser',  sessionMgm.getAllMember());
                // console.log(clients)
            }
        }
    }
}