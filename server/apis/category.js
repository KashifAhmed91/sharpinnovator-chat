/**
 * Created by oDev on 1/19/2015.
 */
exports = module.exports = function(app){
    app.createCategory = function(req,res){
        console.log(req);
    };

    app.getAllCategory = function(req,res){
        var query = app.db.models.Category.find();

            query.exec(function(err,categories){
                if(err){
                    res.send({
                        code:err.code,
                        err:err
                    })
                }else{
                    res.send({
                        code:200,
                        data:categories
                    })
                }
            })
    };

    app.setCategory = function(req,res){
        var query = app.db.models.Category(req.body);
        query.save(function(err,category){
            if(err){
                res.send({
                    code:err.code,
                    err:err
                });
            }else{
                res.send({
                    code:200,
                    data:category
                })
            }
        })
    }

    app.removeCategory = function(req,res){
        var query = app.db.models.Category.where({_id:req.body._id});
        query.remove(function(err,category){
            if(err){
                res.send(err)
            }else{
                res.send({code:category})
            }
        })
    }
};