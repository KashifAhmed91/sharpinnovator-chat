/**
 * Created by oDev on 1/16/2015.
 */
exports = module.exports = function(app,mongoose){
    app.db = mongoose.createConnection(app.get('mongodb-url'));
    app.db.on('error', console.error.bind(console, 'mongoose connection error: '));
    app.db.once('open', function () {
        console.log('mongoose open for business');
    });
};