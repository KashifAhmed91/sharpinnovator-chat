/**
 * Created by oDev on 1/26/2015.
 */
adminApp.factory('SocketService',function($rootScope){

    // var _mySocket = io.connect('http://localhost:8081/');

    var socket = $rootScope.io
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        },
        socket:function(){
            return socket
        }
    };
}).run(function(CooperateService){
    CooperateService.socket.connected();
});