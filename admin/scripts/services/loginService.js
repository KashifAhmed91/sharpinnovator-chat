/**
 * Created by Kashif Ahmed on 1/21/2015.
 */
adminApp.factory('LoginService',function(AjaxService){
    var login = AjaxService.doLogin();
    var professionalLogin = AjaxService.professionalDoLogin();
    var professionalSignUp = AjaxService.professionalDoSignUp();
    return{
        doLogin: function(userObj,successCB,errorCB){
            return login.save(userObj,function(response){
                if(response.code==200){
                    successCB(response)
                }else{
                    errorCB(response)
                }
            })
        },
        professionalLogin: function(userObj,SuccessCb,ErrorCb){
            return professionalLogin.save({email:userObj.email,password:userObj.password},function(response){
                if(response.code==200){
                    SuccessCb(response);
                }else{
                    ErrorCb(response.data);
                }
            })
        },
        professionalSignUp: function(userObj,SuccessCB,ErrorCb){
            return professionalSignUp.save({
                name: userObj.name,
                email: userObj.email,
                password: userObj.password,
                professional: userObj.professional,
                category: userObj.category
            },function(response){
                if(response.code == 200){
                    SuccessCB(response);
                }else{
                    ErrorCb(response.data)
                }
            });
        }
    }
});


/*
return login.save({email:user.email,password:user.password},function(response){
    if(response.code==200){
        SuccessCb(response);
    }else{
        ErrorCb(response.data);
    }
});*/
