/**
 * Created by oDev on 1/23/2015.
 */
adminApp.factory('CategoriesServices',function(AjaxService){
    var getAllCategory  = AjaxService.getAllCategory();
    var delCategory = AjaxService.delCategory();
    var setCategory = AjaxService.addCategory();
    return{
        get:function(successCb,errorCb){
            return getAllCategory.get(null,function(response){
                if(response.code == 200){
                    successCb(response)
                }else{
                    errorCb(response);
                }
            })
        },
        del:function(id,successCb,errorCb){
            return delCategory.save(id,function(res){
                if(res.code){
                    successCb(res);
                }else{
                    errorCb(res);
                }
            })
        },
        set: function(dataObj,successCb,errorCb){
            return setCategory.save(dataObj,function(res){
                if(res.err){
                    errorCb(res)
                }else{
                    successCb(res)
                }
            })
        }

    }
})