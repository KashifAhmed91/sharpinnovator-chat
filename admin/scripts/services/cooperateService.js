/**
 * Created by Kashif Ahmed on 1/21/2015.
 */
adminApp.factory('CooperateService',function(SocketService){
    var _category = {};
    var _user = {};
    var _roomID = "";
    var _chatRoomInfo = {};
    return{
        category: {
            get: function(){
                return _category;
            },
            set: function(category){
                _category = category;
            }
        },
        user : {
            get: function(){
                return _user;
            },
            set: function(user){
                _user = user;
            }
        },
        chattingRoom : {
            get:function(){
                return _roomID;
            },
            set: function(Id){
                _roomID = Id;
            }
        },
        chatRoomInfo:{
            get:function(){
                return _chatRoomInfo;
            },
            set:function(roomInfo){
                _chatRoomInfo = roomInfo;
            }
        },
        socket: {
            connected: function(){
                SocketService.on('connected', function (msg) {
                    console.log(msg)
                })
            },
            reconnectSocket: function(){
                SocketService.on('reconnectSocket', function (msg) {
                    console.log('Reconnect Socket');
                })
            },
            updateSocket: function(){
                SocketService.on('update', function (msg) {
                    console.log(msg)
                })
            },
            errorSocket: function(){
                SocketService.on('error', function (msg) {
                    console.log(msg)
                })
            },

            requestForChat: function(){
                SocketService.on('RequestForChat',function(reqObject){

                })
            },
            resumeChat : function(){
                SocketService.on('RequestForResumeChat',function(resumeChatMsg){
                    console.log('RequestForResumeChat')
                })
            },
            addMeToSocket : function(){
                SocketService.emit('addMeToSocket',{userID:_user._id,key:'D',socketID:SocketService.socket().id});
            },
            addedMeToSocket: function(callBack){
                SocketService.on('userAdded',function(addedUser){
                    callBack(addedUser);
                })
            },
            onRequestForChat: function(callBack){
                SocketService.on('RequestForChat',function(reqUser){
                    callBack(reqUser);
                })
            },
            acceptChatRequest: function(userID){
                SocketService.emit('chatRequestAccepted',{memberID:{memberOne: _user._id, memberTwo: userID}})

            },
            startChat: function(callBack){
                SocketService.on('startChat',function(data){
                    callBack(data);
                })

            }

        }
    }
});

/*
 SocketService.on('connected', function (msg) {
 console.log(msg)

 })
 SocketService.on('RequestForChat', function (msg) {
 //requestingUserData._id,requestingUserData.name

 //here you will generate popup to ask professional for chat

 //if(yes){
 //    SocketService.emit('chatRequestAccepted',{memberID:{memberOne:prof,memberTwo:use}})
 //}else{
 //    SocketService.emit('chatRequestCancelled',{})
 //
 //}


 })
 SocketService.on('RequestForResumeChat', function (msg) {
 console.log('RequestForResumeChat')
 // socketTest.emit('joinTheRoom',msg)

 })
 SocketService.on('message', function (msg) {
 console.log('mg1')
 //var Url = $location.$$path;
 //if(Url == '/inbox/message/'+msg.data.memberID.memberOne)
 //{}else{
 //    $location.path('/inbox/message/'+msg.data.memberID.memberOne)
 //    if(!$scope.$$phase) $scope.$apply();
 //}
 })

 SocketService.on('update', function (msg) {
 console.log(msg)
 })

 SocketService.on('error', function (msg) {
 console.log(msg)
 })

 SocketService.on('reconnectSocket', function (msg) {


 })

 SocketService.on('listOfOnlineUser', function (clients) {
 console.log(clients)
 });*/
