/**
 * Created by oDev on 1/20/2015.
 */
adminApp.factory('AjaxService',function($resource,$rootScope){

    var _doLogin = function(){
        return $resource($rootScope.server+'admin/login',{email:'@email',pass:'@pass'})
    };

    var _professional_doSignUp = function(){
        return $resource($rootScope.server+'user/signUp',{name:'@name',email:'@email',password:'@password',professional:'@professional',category:'@category'})
    };

    var _professional_doLogin = function(){
        return $resource($rootScope.server+'user/login',{email:'@email',password:'@password'})
    };

    var _getAllCategories = function(){
        return $resource($rootScope.server+"user/getCategory")
    };

    var _delCategory = function(){
        return $resource($rootScope.server+'user/deleteCategory',{id:'@id'});
    };

    var _getAllUser = function(){
        return $resource($rootScope.server+'user/getUsers')
    }

    var _addCategory = function(){
        return $resource($rootScope.server+'user/setCategory',{name:'@name',description:'@description'})
    }

    return{
        doLogin: _doLogin,
        professionalDoLogin: _professional_doLogin,
        professionalDoSignUp: _professional_doSignUp,
        getAllCategory: _getAllCategories,
        delCategory: _delCategory,
        getAllUser: _getAllUser,
        addCategory: _addCategory
    }
});