/**
 * Created by kashif on 1/23/2015.
 */
adminApp.factory('UserService',function(AjaxService){
   var getAllUser = AjaxService.getAllUser();

    return{
        getAll: function(SuccessCb,ErrorCb){
            getAllUser.get({},function(userObj){
                if(userObj.code == 200){
                    SuccessCb(userObj);
                }else{
                    ErrorCb(userObj);
                }
            });
        }

    }
});