/**
 * Created by oDev on 1/15/2015.
 */
var adminApp = angular.module('adminApp',['ui.router','ngResource'])
    .config(function($stateProvider, $urlRouterProvider) {
        //
        // For any unmatched url, redirect to /state1
        $urlRouterProvider.otherwise("professional");
        //
        // Now set up the states
        $stateProvider
            .state('admin', {
                url: "/admin",
                templateUrl: "partials/adminLogin.html",
                controller:'LoginController'
            }).state('professional',{
                url:'/professional',
                templateUrl:'partials/professional.html',
                controller:'professionalController'
            }).state('professionalLogin',{
                url: '/login',
                templateUrl: 'partials/professional/login.html',
                controller: 'professionalLoginController'
            }).state('professionalSignUp',{
                url: '/signUp',
                templateUrl: 'partials/professional/signUp.html',
                controller: 'professionalSignUpController'
            })


            .state('panel', {
                url: "/panel",
                abstract: true,
                templateUrl: "partials/panel.html",
                controller:'panelController'
            })
            .state('panel.chatList', {
                url: "/userChatList",
                templateUrl: "partials/panel/chatList.html",
                controller:'ChatListController'
            }).
            state('panel.chatting',{
                url:'/chatting',
                templateUrl: 'partials/panel/chatting.html',
                controller:'chattingController'
            })
            .state('panel.topicList', {
                url: "/list",
                templateUrl: "partials/panel/topicList.html",
                controller:'topicListController'
            }).state('panel.userList', {
                url: "/list",
                templateUrl: "partials/panel/userList.html",
                controller:'userListController'
            }).state('panel.userChat',{
                url:'/userChat',
                templateUrl:'partials/panel/userChat.html',
                controller:'userChatController'
            }).state('panel.categories',{
                url:'/categories',
                templateUrl:'partials/panel/categories.html',
                controller:'categoriesController'
            }).state('panel.addNewCategory',{
                url:'/categories',
                templateUrl:'partials/admin/createNewCategory.html',
                controller:'addCategoryController'
            })
            .state('panel.topic',{
                url:'/topic',
                templateUrl:'partials/panel/topic.html',
                controller:'topicController'
            }).state('panel.category',{
                url:'/category',
                templateUrl:'partials/panel/category.html',
                controller:'categoryController'
            });
    })
        .run(function($rootScope){
            $rootScope.server='http://localhost:8080/';
            $rootScope.io = io.connect($rootScope.server);
        })
    ;