/**
 * Created by oDev on 1/23/2015.
 */
adminApp.controller('professionalSignUpController',function($scope,$state,CategoriesServices,LoginService){
    $scope.doSignUp = function(){
        console.log('Do SignUp')
    }
    $scope.user = {};
    $scope.list=[];
    $scope.selectedCategory = {};

    $scope.categoryList = (function(){
        CategoriesServices.get(function(category){
            console.log("Success",category);
            $scope.categories=category.data;
        },function(error){
            console.log("Error",error);
        })
    })();

    $scope.doSignUp = function(){
        $scope.user.professional = true;
        LoginService.professionalSignUp($scope.user,function(res){
            $state.go('professional')
        },function(res){
            if(res.code == 11000){
                console.log("User Already Exist");
            }else{
                console.log("Try Again")
            }
        })
    };

    $scope.changeCategory = function (selectedCategory) {
        $scope.user.category = {
            name: selectedCategory.name,
            id: selectedCategory._id
        }
    };

    $scope.$watch('user.professional',function(newValue){
        if(!newValue){
            $scope.user.category = {};
            console.log('Disable Category');
        }else{
            console.log('Enable Category');
        }
    });
})