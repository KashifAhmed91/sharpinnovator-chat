/**
 * Created by oDev on 1/23/2015.
 */
adminApp.controller('professionalLoginController',function($scope,LoginService,$state,CooperateService){
    $scope.user = {};
    $scope.doLogin = function(){
        console.log("Professional Login");
        LoginService.professionalLogin($scope.user,function(response){
                if(response.code==200){
                    console.log("Login Successfully");

                    if(response.data.professional){
                        CooperateService.user.set(response.data);
                        CooperateService.socket.addMeToSocket();
                        CooperateService.socket.addedMeToSocket(function(data){
                            $state.go('panel.chatList');
                        });
                    }
                }
            }
            ,function(error){
                alert("Password and User not correct")
            }
        )
    }
});