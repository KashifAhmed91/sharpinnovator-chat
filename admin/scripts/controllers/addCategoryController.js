/**
 * Created by kashif on 1/28/2015.
 */
adminApp.controller('addCategoryController',function($scope,CategoriesServices){
    $scope.category = {};
    console.log('add Category Controller');
    $scope.addCategory = function(){
        CategoriesServices.set($scope.category,function(res){
            if(res.code==200){
                console.log("Category Successfully saved");
                $scope.category = {};
            }
        },function(err){
            console.log("Error in Category Save",err);
        });
    }
});