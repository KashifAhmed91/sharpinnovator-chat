/**
 * Created by oDev on 1/15/2015.
 */
adminApp.controller('userListController',function($scope,UserService){
   $scope.users = []
   UserService.getAll(function(user){
       $scope.users = user.data;
   },function(error){
       console.log(error)
   })
});