/**
 * Created by oDev on 1/15/2015.
 */
adminApp.controller('ChatListController',function($scope,CooperateService,$state){
    //console.log("Chat List Controller Call");
    $scope.chatReqUser = [];

    $scope.acceptChatRequest = function(userID){
        CooperateService.socket.acceptChatRequest(userID)
    };

    $scope.rejectChatRequest = function(userObj){

    };

    CooperateService.socket.startChat(function(roomInfo){
        CooperateService.chatRoomInfo.set(roomInfo)
        $state.go('panel.chatting');

    });
    CooperateService.socket.onRequestForChat(function(reqUserObj){
        /*alert("Chat Request");
         console.log('reqUserObj',reqUserObj);*/
        $scope.chatReqUser.push(reqUserObj.requestingUserData);
    });



});