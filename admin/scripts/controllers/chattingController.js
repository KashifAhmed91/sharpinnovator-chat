/**
 * Created by oDev on 2/2/2015.
 */
adminApp.controller('chattingController',function($scope,CooperateService,SocketService){
   console.log('chattingController');


    console.log('Chat Room info',CooperateService.chatRoomInfo.get());
    console.log('UserInfo',CooperateService.user.get());

    $scope.messageData={}
    $scope.sendMessage =function(){
        var roonmInfo = CooperateService.chatRoomInfo.get()
        $scope.messageData.senderID = (CooperateService.user.get())._id
        $scope.messageData.reciverID = roonmInfo.memberID[1];
        $scope.messageData.roomID = roonmInfo.roomID;

        SocketService.emit('HostSendMessage',$scope.messageData)

    }

    SocketService.on('messageDelivered',function(data){
        console.log(data.msg)
        console.log(data.data)
    })
    SocketService.on('message',function(data){
        console.log(data.msg)
        console.log(data.data)
    })

    SocketService.on('errorInSendingMessage',function(data){
        console.log(data.msg)
        console.log(data.data)
    })

});