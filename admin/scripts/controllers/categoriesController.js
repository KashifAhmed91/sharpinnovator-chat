/**
 * Created by oDev on 1/15/2015.
 */
adminApp.controller('categoriesController',function($scope,CategoriesServices,$state,CooperateService){
    $scope.categoryList = [];
    refreshCategory();
    $scope.changeState = function(state,category){
        CooperateService.category.set(category);
        $state.go(state);
    };

    $scope.removeCategory = function(id){
        CategoriesServices.del(id,function(response){
            refreshCategory();
        },function(error){
            console.log(error)
        })
    };

    $scope.createNewCategory = function(){
        $state.go('panel.addNewCategory');
    };

    /*Method*/
    function refreshCategory(){
        CategoriesServices.get(function(response){
            $scope.categoryList = response.data
        },function(error){
            console.log(error)
        });
    }
});